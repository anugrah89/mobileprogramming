// No 1 ES6 Mengubah fungsi menjadi fungsi arrow by @anugrah89 181351027
console.log("Jawaban No.1:");
const golden = () => {
  console.log("this is golden!!")
}
 
golden();

// No 2 ES6 Sederhanakan menjadi Object literal di ES6 by @anugrah89 181351027
console.log("\nJawaban No.2:");
const newFunction = (firstName, lastName) => {
    return{
		fullName(){
			return console.log(firstName + " " + lastName);
		}
	}
}

//Driver Code 
newFunction("William", "Imoh").fullName();

//No 3 ES6 Destructuring by @anugrah89 181351027
console.log("\nJawaban No.3:");
const newObject = {
  firstName: "Harry",
  lastName: "Potter Holt",
  destination: "Hogwarts React Conf",
  occupation: "Deve-wizard Avocado",
  spell: "Vimulus Renderus!!!"
}
const {firstName, lastName, destination, occupation} = newObject;

// Driver code
console.log(firstName, lastName, destination, occupation)

// No 4 ES6 Array Spreading by @anugrah89 181351027
console.log("\nJawaban No.4:");
const west = ["Will", "Chris", "Sam", "Holly"]
const east = ["Gill", "Brian", "Noel", "Maggie"]
const combined = [...west, ...east]
//Driver Code
console.log(combined)

// No 5 ES6 Template Literals by @anugrah89 181351027
console.log("\nJawaban No.5:");
const planet = 'earth'
const view = 'glass'
var before = `Lorem ${view} dolor sit amet, consectetur adipiscing elit ${planet} do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam`
 
// Driver Code
console.log(before) 