// Looping while-loop
console.log('LOOPING PERTAMA')
var loop = 1 ;
var jumlah = 0;
while(loop <= 10){
	jumlah1 = jumlah + loop;
  console.log(loop + jumlah1 +' - I love coding');
loop++
}
console.log('LOOPING KEDUA')
while(loop >= 2){
	jumlah2 = loop-2;
  console.log(loop + jumlah2 + ' - I will become a mobile developer');
loop--
}

// Looping for
console.log('\n')
for (var nomor = 1; nomor <= 20; nomor++){
	if ( nomor % 2 == 1 && nomor % 3 == 0 ) {
		console.log(nomor + ' - I Love Coding')
    } else if (nomor % 2 == 1) {
		console.log(nomor + ' - Teknik')
	}else {
		console.log(nomor + ' - Informatika')
	}	
	          
}

// Persegi Panjang #
console.log('\n')
class Persegi {
	run(n,m){
		for(var i = 1; i <= n; i++){
			var x = '';
		for (var j = 1; j <= m; j++){
			var x = x + '#';
		}
		console.log(x);
		}
	}
}
var persegi = new Persegi;
persegi.run(4,8);
console.log('')	

// Tangga #
console.log('\n')
var s = '';
for( var i = 0; i < 7; i++ ) {
	for( var j = 0; j<= i; j++ ) {
		s += '#';
	}
	s += '\n';
}
console.log(s);

// Papan Catur #
class Catur{
	run(n,m){
		for(var i = 1; i <= n; i++){
			if (i % 2 == 0){
				var x = '';
			} else{
				var x = ' ';
			}
			for(var j = 1; j <= m; j++){
				if (j % 2 == 0){
					var x = x + ' ';
				} else {
					var x = x + '#';
				}
			}
			console.log(x);
		}
	}
}
var catur = new Catur;
catur.run(8,8);