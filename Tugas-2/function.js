// Jawaban Function No 1
console.log("Jawaban no 1:");
function teriak() {
  return "Halo Humanika!\n";
}
console.log(teriak());

// Jawaban Function No 2
console.log("Jawaban no.2:");
function kalikan(x,y) {
  return x*y;
}
 
var num1 = 12;
var num2 = 4;
 
var hasilKali = kalikan(num1, num2);
console.log(hasilKali);

// Jawaban Function No 3
console.log("Jawaban no.3:");
function introduce(name,age,address,hobby) {
  return "\nNama saya "+ name + ", umur saya " + age + " tahun, alamat saya di " + address + " ,dan saya punya hobby yaitu " + hobby + "!";
}
 
var name = "Agus";
var age = 30;
var address = "Jln. Malioboro, Yogyakarta";
var hobby = "Gaming";
 
var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);
