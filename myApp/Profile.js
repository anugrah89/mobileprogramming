import React, {Component} from 'react';
import {
  View, ScrollView, TouchableHighlight, TouchableOpacity, Image, Text, StyleSheet
} from 'react-native';


export default class Profile extends Component <{}> {
  render(){
    return (
      <View style={styles.container}>
        <ScrollView>
            <View style={{padding:10,width:'100%',backgroundColor:'#00838f', height:150}}>
            </View>
            <View style={{alignItems:'center'}}>
              <Image source={require('./assets/fotoprofil.jpg')} style={{width:140,height:140,borderRadius:100,marginTop:-70}}></Image>
              <Text style={{fontSize:25, fontWeight:'bold', padding:10}}>Hello! Saya Anugrah</Text>
              <Text style={{fontSize:20, fontWeight:'bold', color:'white'}}>Teknik Informatika 2018 </Text>
              <Text style={{fontSize:20, fontWeight:'bold', color:'white'}}>Kelas IF/MALAM/B </Text>
              <Text style={{fontSize:20, fontWeight:'bold', color:'white'}}>STT WASTUKANCANA </Text>
              <Text style={{fontSize:20, fontWeight:'bold', padding:10}}>Follow me on social media :</Text>
            </View>
             <View style={{
              alignSelf :'center',
              flexDirection:'row',
              justifyContent:'center',
              backgroundColor:'#fff',
              width:'90%',
              padding:20,
              paddingBottom:22,
              borderRadius:10,
              shadowOpacity:80,
              elevation:15,
              marginTop:20
              }}>
                <Image source={require('./assets/fb.png')}
                style={{width:25,height:25}}></Image>
                <Text style={{fontSize:15,color:'#2962ff',fontWeight:'bold',marginLeft:10}}>fb.com/anugrah89</Text>
              </View>
              <View style={{
              alignSelf :'center',
              flexDirection:'row',
              justifyContent:'center',
              backgroundColor:'#fff',
              width:'90%',
              padding:20,
              paddingBottom:22,
              borderRadius:10,
              shadowOpacity:80,
              elevation:15,
              marginTop:20
              }}>
                <Image source={require('./assets/tw.png')}
                style={{width:25,height:25}}></Image>
                <Text Text style={{fontSize:15,color:'#2962ff',fontWeight:'bold',marginLeft:10}}>twitter.com/anugrahmi89</Text>
              </View>
              <View style={{
              alignSelf :'center',
              flexDirection:'row',
              justifyContent:'center',
              backgroundColor:'#fff',
              color:'ffffff',
              width:'90%',
              padding:20,
              paddingBottom:22,
              borderRadius:10,
              shadowOpacity:80,
              elevation:15,
              marginTop:20
              }}>
                <Image source={require('./assets/ig.png')}
                style={{width:25,height:25}}></Image>
                <Text Text style={{fontSize:15,color:'#2962ff',fontWeight:'bold',marginLeft:10}}>instagram.com/anugrah89</Text>
              </View>
              <View style={{
              alignSelf :'center',
              flexDirection:'row',
              justifyContent:'center',
              width:'90%',
              padding:20,
              paddingBottom:22,
              borderRadius:10,
              shadowOpacity:80,
              elevation:15,
              marginTop:20,
              marginBottom:30,
              backgroundColor:'#005661'
              }}>
                <Text Text style={{fontSize:15,color:'#ffffff',fontWeight:'bold',marginLeft:10, alignItems:'center'}}>Logout</Text>
              </View>
        </ScrollView>
      </View>
    )
  }
}

const styles = StyleSheet.create ({
  container : {
    backgroundColor:'#00838f',
    flex : 1,
    alignItems:'center',
    justifyContent : 'center'
  }
});