import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  TextInput,
  TouchableOpacity
} from 'react-native';

export default class Logo extends Component<{}> {
  render(){
    return(
      <View style={styles.container}>
        <TextInput style={styles.inputBox} 
          underlineColorAndroid='rgba(0,0,0,0)'
          placeholder="Email/Username"
          placeholderTextColor = "#ffffff"
        />
        <TextInput style={styles.inputBox} 
          underlineColorAndroid='rgba(0,0,0,0)'
          placeholder="Password"
          secureTextEntry={true}
          placeholderTextColor = "#ffffff"
        />

        <TouchableOpacity style={styles.button}>
          <Text style={styles.buttonText}>Login</Text>
        </TouchableOpacity>

        <View style={styles.signupTextCont}>
              <Text style= {styles.signupText}>Don't have an account yet?</Text>
              <Text style= {styles.signupButton}> Sign Up</Text>
            </View>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container : {
    flex : 1,
    justifyContent : 'center',
    alignItems: 'center'
  },

  inputBox : {
    width:300,
    height: 35,
    backgroundColor:'rgba(255, 255, 255, 0.3)',
    borderRadius: 25,
    paddingHorizontal:16,
    fontSize:16,
    color:'#ffffff',
    marginVertical:10
  },

  button : {
    width:300,
    backgroundColor:'#005662',
    borderRadius: 25,
    marginVertical:5,
    paddingVertical:10,
    fontWeight:'bold'
  },

  buttonText : {
    fontSize:16,
    fontWeight:'700',
    color:'#ffffff',
    textAlign:'center'
  },
  signupTextCont : {
    flex : 1,
    alignItems:'center',
    justifyContent:'center',
    paddingVertical : 16,
    flexDirection :'row'
  },
  signupText : {
     color:'rgba(255,255,255,0.7)',
    fontSize:16
  },
  signupButton : {
    color : '#ffffff',
    fontSize : 16,
    fontWeight : '700'
  }

});