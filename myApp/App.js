import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  StatusBar
} from 'react-native';

import Login from './Login';
import Profile from './Profile'

export default class App extends Component<{}> {
  render(){
    return (
      <View style={styles.container}>
      <StatusBar
        backgroundColor="#005662"
        barStyle="light-content"/>
        <Login/>
        <Profile/>
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container : {
    backgroundColor:'#00838f',
    flex : 1,
    alignItems:'center',
    justifyContent : 'center'
  }
});